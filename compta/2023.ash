++ SociétéGénérale (Order,Transfer,Cash,Check): 13361.08
++ PayPal (PayPal): 2617.41

-- Janvier


2023/01/05 -42.00€ Order Domiciliation X

2023/01/05 -397.94€ Order Domiciliation X
    MAIF

2023/01/10 5412.60€ PayPal PyconFr2023 X
    PSF grant (6000 $)

2023/01/11 1208.00€ Transfer Cotisation X
    Cotisation + PyConFr

2023/01/16 1000.00€ Transfer PyconFr2023 X
    CGWIRE

2023/01/20 2000.00€ Transfer PyconFr2023 X
    GITGUARDIAN

2023/01/23 -200.00€ Transfer PyconFr2023 X
    BOURSE

2023/01/23 -174.10€ Transfer PyconFr2023 X
    BOURSE

2023/01/25 -13.00€ Order Banque X

2023/01/31 -2262.00€ Transfer PyconFr2023 X
    TSHIRT MISTERUGBY

2023/01/31 -275.93€ Transfer PyconFr2023 X
    ECOCUP REUZ

2023/01/31 2000.00€ Transfer PyconFr2023 X
    SPONSORING ALMA

2023/01/31 500.00€ Transfer PyconFr2023 X
    SPONSORING Mozilla

2023/01/31 -403.98€ PayPal PyconFr2023 X
    BOURSE

-- Février

2023/02/04 -42.00€ Order Domiciliation X

2023/02/06 -1213.92€ Transfer PyconFr2023 X
    TOUR DE COUP AFPY

2023/02/06 -377.88€ Transfer PyconFr2023 X
    BOURSE

2023/02/09 -4999.32€ Transfer PyconFr2023 X
    DEJEUNER PANIER DE LEA

2023/02/10 3508.00€ Transfer Cotisation X
    Cotisation + PyConFr

2023/02/13 2000.00€ Transfer PyconFr2023 X
    SPONSORING Clevercloud

2023/02/13 -400.00€ Transfer PyconFr2023 X
    BOURSE

2023/02/13 -200.00€ Transfer PyconFr2023 X
    BOURSE
