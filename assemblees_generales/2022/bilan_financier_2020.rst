+---------------------+------------+
| Recettes            |    2310.00 |
+---------------------+------------+
| Dépenses            |    2530.78 |
+---------------------+------------+
| Resultat            |    -220.78 |
+---------------------+------------+

Recettes
========

+---------------------+------------+
| Cotisation          |    1030.00 |
+---------------------+------------+
| PyConFr18           |    1000.00 |
+---------------------+------------+
| Don                 |     280.00 |
+---------------------+------------+
| Total               |    2310.00 |
+---------------------+------------+

Dépenses
========

+---------------------+------------+
| Hébergement(serveur)|     714.80 |
+---------------------+------------+
| Domiciliation       |     504.00 |
+---------------------+------------+
| Sponsor             |     500.00 |
+---------------------+------------+
| Assurance           |     381.41 |
+---------------------+------------+
| Meetup              |     181.77 |
+---------------------+------------+
| Banque              |     148.80 |
+---------------------+------------+
| April               |     100.00 |
+---------------------+------------+
| Total               |    2530.78 |
+---------------------+------------+

Soldes
======

+---------------------+------------+
| SociétéGénérale     |    5528.71 |
+---------------------+------------+
| PayPal              |    2821.27 |
+---------------------+------------+
| Total               |    8349.98 |
+---------------------+------------+
