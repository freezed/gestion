
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 15 Mars 2023



**Début prévu : 20h00**



Présents :

- [x] Marc Debureaux

- [x] Lucie Anglade

- [x] Thomas Bouchet

- [] Bruno Bonfils

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [x] Jean Lapostolle

- [x] Nicolas Ledez



- [ ] Hervé Sousset

- [] JeffD

- [x] Julien Palard





## Actions prévues lors de la réunion précédente



   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec gandi pour l'interview et obtenir les questions
   * Pierre : Prendre contact avec Hévéa pour la gestion du courrier et savoir ce qui est envisageable → fait (Arthur a la clef et récupère le courrier actuellement)




## Ordre du jour

   * Les premiers résultats du sondage : 80 réponses pour le moment, ouvert jusqu'au 15 avril
       * 70 hommes / 9 femmes / 1 pas de réponse
       * 36% au sprints, 12% ateliers, 94% participent aux conférences
       * note globale pour l'event : 3,42/4
       * programme : 3,07/4
       * soirée : 3/4  (NC : 47)
       * comm : 2,83 / 4 (NC : 8)
       * médiation : 83% de oui et 12% de non (NC : )
       * harcèlement : non
           * 1 plainte sur le point médian à "harcelé·e" pour raison de dysléxie
           * 1 personne qui a dit qu'elle n'aurait pas fait de conf s'il n'y avait pas eu de telle charte
       * améliorations : 
           * afficher un flag FR/EN pour les confs
           * niveau de compétences nécessaire pour les confs
           * buffet parfait / pas assez à manger (ou fini trop tôt)
           * confs peut-être pas assez poussées
           * confs trop orientées data
           * plus de points "événement social"  (soirée jeu le vendredi soir ?)
           * soirée du samedi : aider les gens à parler entre eux pour aider les rencontres
           * food-truck : plus de choix veggie / vegan 
           * Merci et bravo
           * confs supers, soirée moins bien
           * ajouter une présentation des orateurs dans le programme (bio ?)
           * laisser du temps pour les changements de salle
           * sous-entendus contre les utilisateurs macOS et IRC/Discord
       * Fin du sondage : faire un article récapitulatif sur le site => Lucie
   * Un dossier pour les rapports du comité diversité dans le dépôt de gestion
       * Un peu dur de trouver les rapports diversité des années précédentes, ce serait pratique qu'ils soient publiés dans le dépôt afpy\_gestion. On pourrait au moins mettre ceux de 2019 que Laurine a retrouvé + celui de 2023
       * 2019: [https://www.pycon.fr/2019/static/images/2019\_pyconfr\_transparency\_report.pdf](https://www.pycon.fr/2019/static/images/2019\_pyconfr\_transparency\_report.pdf)
       * 2018: [https://www.pycon.fr/2018/fr/images/PyConFrance2018\_CodeOfConduct\_Transparency\_report.pdf](https://www.pycon.fr/2018/fr/images/PyConFrance2018\_CodeOfConduct\_Transparency\_report.pdf) et [https://www.pycon.fr/2018/en/images/PyConFrance2018\_CodeOfConduct\_Transparency\_report.pdf](https://www.pycon.fr/2018/en/images/PyConFrance2018\_CodeOfConduct\_Transparency\_report.pdf)
   * Transfert des mots de passe toussa du président
       * Modification du paramétrage du Discord
   * Les sujets qu’on voudrait aborder avec l’interview Gandi
       * Traduction de la doc python <3
       * Freins à la diffusion du langage
       * Aller les voir directement pour l'interview ? (Marc)
       * Sponsoring / Interaction en tant qu'association
       * Nouvelles interactions avec l'EuroPython
       * Nos événements sont gratuits
       * HackInScience ?
   * AFPy × PyLadies sur les meetups de Lyon
       * La comm dit actuellement que les meetups sont organisés par l'AFPy et CourtBouillon, avec charte "ne pas être débile"
       * Organiser un meet'up avec les PyLadies (sans hommes cis), est-ce que ça peut être organisé avec le soutien de l'AFPy ?
       * Peut aider à donner la parole aux femmes
       * Permet de présenter l'AFPy comme une asso safe / ouverte
       * Essayer de faire une keynote sur le sujet à la prochaine pycon ?
       * → le CD approuve l'organisation suite à un vote
   * Finalisation du compte-rendu d'AGO
       * Compte-rendu écrit et PR ouverte : [https://git.afpy.org/AFPy/gestion/pulls/2](https://git.afpy.org/AFPy/gestion/pulls/2)
       * Manquent les bilans financiers
       * Reste à intégrer la mise à jour des statuts, signer (présidente + secrétaire ?) et envoyer à la préfecture (😨)
   * Compta : A finir de payer (l’université, octopuce)  = -2593.72
       * bilan pour l’instant:  + 3877.25
       * reste sur le compte  : 7 161,42 EUR
       * et en plus on a 6000 $ + 3 184,08 € sur PayPal


## Actions

   * Lucie : Écrire un article récapitulatif sur le site de la pycon suite aux résultats du sondage
   * Lucie : mettre les compte-rendus de diversité sur gitea
   * Marc : Interview Gandi
   * Pierre : mettre les bilans financiers dans le compte-rendu de l’AG
   * Antoine : finaliser le compte-rendu + déclaration de l'AGO
   * Melcore : Ouvrir un sujet-wiki sur discuss pour répertorier le matériel qu'on a suite à la pycon (adaptateurs, stickers, etc.)




Prochaine réunion le 19 avril à 20h00



**Fin : 21h22**


