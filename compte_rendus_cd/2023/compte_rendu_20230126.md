
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 26 Janvier 2023



**Début prévu : 20h00**



Présents :

- [x] Marc Debureaux
- [] Jules Lasne
- [x] Lucie Anglade
- [x] Thomas Bouchet
- [] Bruno Bonfils
- [x] Pierre Bousquié
- [x] Antoine Rozo
- [x] Laurine Leulliette
- [x] Jean Lapostolle



- [x] Hervé Sousset
- [] JeffD
- [x] Julien Palard
- [x] Florian Guillet
- [] Marcos Medrano
- [x] Vincent Delecroix





## Actions prévues lors de la réunion précédente



   * Antoine/Marc : Faire un post sur discuss pour établir une liste des exigences que l'on a pour des locaux PyConFr (matériel nécessaire, disponibilités en calendrier, nombre de places, etc.). Au plus fort de la journée à Bordeaux on avait 666 personnes (moins qu'à Lille où on avait dépassé les 800). Partager avec les universités un document formel incluant ces exigences et des photos des événements. → en cours pour le moment rien
   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec gandi pour l'interview et obtenir les questions
   * Pierre : Prendre contact avec Hévéa pour la gestion du courrier et savoir ce qui est envisageable → en cours
   * Pierre : Regarder les factures des années précédentes pour voir à quels imprimeurs on faisait appel et quels sont les tarifs habituels
   * Jeff : voir pour imprimer des affiches PyConFr et Afpy pour la fosdem qui aura lieu début février
   * Lucie : fait le programme avec Guillaume
   * Marc : envoyer la convocation avant le 19 janvier
   * Jean : Répondre à Taipy
   * Âme charitable : rappeler à Marc si il a pas fait de convocation d’ici le 16 janvier
   * Jean : Retardataire : Leur proposer de faire un Lightning Talk ou trou dans le planning
   * Mindiell : les t-shirts
   * Marc : voir avec Yoann pour le retour du matériel du buffet
   * Laurine et Lucie : voir l’orga pour le comité diversité




## Ordre du jour

   * Le petit-déjeuner le week-end, on en est où avec les Paniers de Léa ? → Devis de 7k€, devrait pouvoir le faire niveau trésorerie (mais serré)
   * Le café et l’eau pour les sprints ? Lors de l’édition précédente, Les Paniers de Léa avaient fourni le café pour les sprints, mais comme ils ne sont plus sur Bordeaux, ça me semble un peu compliqué pour cette année → Vincent peut avoir deux percolateurs de 10L pour le café et acheter le café
   * Est-ce qu’on a des écocups pour la journée ? → on peut acheter des gobelets carton sur place (il y a un supermarché pas loin de l'université) ou ecocup.com (234€ pour 300 gobelets 25cl, livrables en 1 semaine, avec le logo du t-shirt) → faire un devis
   * Les goodies ? → ecocups et t-shirts sont suffisants ? Demander des stickers à stickermule et linuxmag pour les magazines
   * T-shirts : limiter à 100 la vente en ligne mais préciser qu'ils seront en vente sur place à 20€, et commande de 150 (100 vendus en ligne + 50 sur place)
   * Est-ce qu’on est bon niveau pcs de présentateur ? [https://discuss.afpy.org/t/pyconfr-2023-la-todo-wiki/879/105](https://discuss.afpy.org/t/pyconfr-2023-la-todo-wiki/879/105) → se renseigner auprès de l'université pour la connectique (VGA ? HDMI ?), Yoann peut avoir des PC sur place mais voir si besoin de connectique
   * Matériel : Mindiell a 5 convertisseurs VGA→HDMI, 1 télécommande Targus et 10 feutres noirs, Marc a 1 convertisseur
   * Est-ce que quelqu’un aura une voiture pour si il faut faire des courses au dernier moment ? Oui Pierre Bousquié
   * Impression des affiches ? → Il faut envoyer les logos des différents sponsors pour qu'ils soient mis sur l'affiche. Mais c'est moins urgent (ça peut encore être fait la veille au pire). L'affichee est déjà prête avec des placeholders pour les logos.
   * Tours de cou : Livraison au labo de Vincent à l'université
   * Est-ce qu’on vend aussi des bouteilles de vin comme les autres années pendant la soirée en plus des boissons fournies par le traiteur ? Voir au dernier moment si besoin
   * Des gens motivés pour faire partie du comité diversité pour la soirée ? → appel dédié pendant les sprints voire en ouverture de plénière pour avoir + de 2 personnes pour gérer la soirée
       * Mindiell : ok pour la soirée (et plus si besoin)
   * Quel est l’amphi le plus excentré ? Amphi Darwin ([https://cartographie.u-bordeaux.fr/TPG\_ZoomTalence.html)](https://cartographie.u-bordeaux.fr/TPG\_ZoomTalence.html)), pour dédier aux lightning talks. Ou plutôt dédier le Wegener pour les lightning talks (et conférences d'ouverture/fermeture) car il est séparé du reste
   * Point planning (les trous, les conférences encore en attente…) → attendre la semaine prochaine pour la communication du planning
   * Note surtout pour l’AG : c’est un tiers des membres du CD qui doit être renouvelé à chaque AG, si y a pas de démissionnaires, c’est par date d’entrée la plus ancienne (les sortant·e·s sont Marc, Lucie et Jules)
   * Devis à signer / valider:
       * Devis Association Raffut (Captation Vidéo)
       * Devis octopuce (streaming)
       * Devis Paniers de Léa
   * accès internet sur les différents sites? (besoin d'un accès internet, débridé si possible)




# Stats d'inscrits



[https://pad.chapril.org/p/inscriptions-pycon-fr-2023](https://pad.chapril.org/p/inscriptions-pycon-fr-2023)   <= pas à jour ? Si si julien le met à jour "de temps en temps".



## Actions

   * Vincent appelle le service evenements de l'université demain vendredi 27 pour : emplacement événement (A22 ?), emplacement food truck + liste des salles pour accès test audio/internet, et connectique +~~ réception des colis,~~ + connexion Internet + remise du matériel après la soirée
   * reception des colis: envoyer à Vincent [https://www.labri.fr/perso/vdelecro/contact.html](https://www.labri.fr/perso/vdelecro/contact.html)
   * Antoine : commande ecocups (paiement possible par paypal) → livraison au labo de Vincent (adresse à [https://www.labri.fr/perso/vdelecro/contact.html)](https://www.labri.fr/perso/vdelecro/contact.html))
   * Marc: contacter stickermule pour stickers "génériques" + sticker pyconfr
   * Mindiell : commande des 150 t-shirts sérigraphiés, livraison au 11 février
   * Antoine : Relancer Alma pour le sponsoring (pour que le logo puisse être présent sur les affiches)
   * Marc : contacter les collectivités territoriales pour la comm' autour de l'événement, avec une affiche sans sponsors (PNG, PDF)
   * Pierre : acheter le vin pour la soirée s'il reste du budget


Prochaine réunion le 19 février à 9h00 (Assemblée générale)



**Fin : 21h22**
